package listas;

public class Ejecutable {
    public static  void main(String args[]){
        ListaCircular lista = new ListaCircular();
        System.out.println("Lista vacia: ");
        lista.agregar(1);
        lista.agregar(2);
        lista.agregar(3);
        lista.agregar(4);
        lista.agregar(5);
        lista.agregar(6);
        lista.agregar(8);
        lista.agregar(7);
        lista.agregar(9);
        lista.verLista();
        System.out.println("Buscando un dato ");
        Nodo encontrado = lista.buscar(8);
        if(encontrado != null) {
            System.out.println("El dato encontrado es: " + encontrado.getDato());
        }
        else {
            System.out.println("No se encontró el dato.");
        }
        lista.eliminar(8);
        lista.verLista();
    }
}
