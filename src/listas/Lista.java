package listas;

public interface Lista {
    /**
     * Agrega el dato al principio de la lista.
     * @param dato Tipo int.
     **/
    void agregar(int dato);

    /**
     * Encuentra un nodo con un dato específico.
     * @param dato Tipo int.
     * @return
     */
    public Nodo buscar(int dato);

    /**
     * Borra un nodo indicado por el dato.
     * @param dato Tipo int.
     * @return Regresa verdadero si realiza la operación, sino regresa falso.
     */
    boolean eliminar(int dato);
    void verLista();
}
