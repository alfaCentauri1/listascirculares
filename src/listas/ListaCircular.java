package listas;

public class ListaCircular implements Lista{
    private Nodo primero;
    private Nodo ultimo;
    private int tamanio;

    public ListaCircular() {
        primero = null;
        ultimo = null;
        tamanio = 0;
    }

    /**
     * Agrega el dato al principio de la lista.
     *
     * @param dato Tipo int.
     **/
    public void agregar(int dato) {
        Nodo nuevo = new Nodo(dato);
        if (tamanio == 0){
            primero = nuevo;
            ultimo = nuevo;
        }
        else{
            nuevo.setAnterior(ultimo);
            ultimo.setSiguiente(nuevo);
            nuevo.setSiguiente(primero);
            nuevo.setAnterior(ultimo);
            ultimo = nuevo;
            primero.setAnterior(ultimo);
        }
        tamanio++;
    }

    /**
     * Encuentra un nodo con un dato específico.
     *
     * @param dato Tipo int.
     * @return
     */
    @Override
    public Nodo buscar(int dato) {
        Nodo encontrado = null, actual = primero;
        if(actual != null){
            do{
                if (dato == actual.getDato())
                    encontrado = actual;

                actual = actual.getSiguiente();
            }while(actual != primero && encontrado == null);
        }
        return encontrado;
    }

    /**
     * Borra un nodo indicado por el dato.
     *
     * @param dato Tipo int.
     * @return Regresa verdadero si realiza la operación, sino regresa falso.
     */
    @Override
    public boolean eliminar(int dato) {
        boolean encontrado = false;
        Nodo actual = new Nodo();
        Nodo anterior = new Nodo();
        actual = primero;
        anterior = ultimo;
        do{
            if (actual.getDato() == dato){
                if(actual == primero){
                    primero = primero.getSiguiente();
                    ultimo.setSiguiente(ultimo);
                    primero.setAnterior(ultimo);
                }
                else {
                    if (actual == ultimo){
                        ultimo = anterior;
                        primero.setAnterior(ultimo);
                        ultimo.setSiguiente(primero);
                    }
                    else {
                        anterior.setSiguiente(actual.getSiguiente());
                        actual.getSiguiente().setAnterior(anterior);
                    }
                }
                encontrado = true;
            }
            anterior = actual;
            actual = actual.getSiguiente();
        }while(actual != primero);
        return encontrado;
    }

    @Override
    public void verLista() {
        Nodo actual = primero;
        do{
            System.out.println("Dato: " + actual.getDato());
            actual = actual.getSiguiente();
        }while(actual != primero);
        System.out.println("Fin.");
    }
}
